<div id="top"></div>

<!--
** Inspired by the Best-README-Template.
** Let's create something AMAZING! :D

** GitLab Flavored Markdown - https://gitlab.com/gitlab-org/gitlab/-/blob/master/doc/user/markdown.md

PROJECT SHIELDS
** https://shields.io/
-->


<div align="center">
  <h1>{{ service_name }}</h1>
</div>

[[_TOC_]]

## 📍 About The Project

Here's a blank template to get started. Below is a To Do list to make sure all gaps and variables are fulfilled.

- [ ] Fill in the GitLab project description section. It supposes to be approximately one sentence long.
- [ ] Review all the sections and modify them to reflect the repo status. Feel free to add other sections, but keep sections *About*, *Tags* and *Ownership* as they are used in [Project audit](https://audit-staging.core.morressier.com/)  :raised_hands:
- [ ] Replace this To Do list with a fuller description of the current repo. Let everyone know what the project is about and what its purpose is.


### 🧑‍💻 Ownership

>**Owner Name**
>
> Slack: [@owner_name](https://morressier.slack.com/team/memberId) <br/>
> Email: [user@example.com](mailto:user@example.com)

<br/>

>**Shadow Owner Name**
>
> Slack: [@shadow_owner_name](https://morressier.slack.com/team/memberId) <br/>
> Email: [user@example.com](mailto:user@example.com)


<!-- TAGS
** provide here labels for this project like team, domain, service, and any other valuable information for the project audit
-->
### Tags

`#template` `#some other tag` `#email-worker` `#core-team` `#emails-domain` `#a/b testing domain`

<!-- DEPENDENCIES
** Include dependencies on other services (internal or external)
-->
### Dependencies

- [platform-gateway](https://gitlab.com/morressier/gateways/platform-gateway)
- [![TypeScript][TypeScript-badge]][TypeScript-url]
- [![React][React.js-badge]][React-url]

### Databases

- events-manager (mongo)
- abstract-manager (mongo)
- content-library (mongo)
- email-worker (mongo)
- redis
- algolia

<div align="right">
  <p>(<a href="#top">back to top</a>)</p>
</div>

<!-- GETTING STARTED
** Include any instructions that would be useful
    to get this project up and running
    as well as info about troubleshooting known issues
-->
## 🚀 Getting Started

This is an example of how you may give instructions on setting up your project locally.
To get a local copy up and running, follow these simple example steps.

### Prerequisites

This is an example of how to list things you need to use the software and how to install them.
* npm
  ```sh
  npm install npm@latest -g
  ```


### Installation

1. Get a free API Key at [https://example.com](https://example.com)
2. Clone the repo
   ```sh
   git clone git@gitlab.com:morressier/gitlab_group_name/repo_name.git
   ```
3. Install NPM packages
   ```sh
   npm install
   ```

<div align="right">
  <p>(<a href="#top">back to top</a>)</p>
</div>


## 🕹 Usage

Use this space to show useful examples of how a project can be used. Additional screenshots, code examples and demos work well in this space. You may also link to more resources.

_For more examples, please refer to the [Documentation](https://example.com)_

[![Product Name Screen Shot](images/screenshot.png)](https://example.com)

<div align="right">
  <p>(<a href="#top">back to top</a>)</p>
</div>

<!-- MARKDOWN LINKS & IMAGES -->
<!-- https://www.markdownguide.org/basic-syntax/#reference-style-links -->

[TypeScript-badge]: https://img.shields.io/badge/TypeScript-3178C6?style=for-the-badge&logo=typescript&logoColor=white
[TypeScript-url]: https://nextjs.org/
[React.js-badge]: https://img.shields.io/badge/React-20232A?style=for-the-badge&logo=react&logoColor=61DAFB
[React-url]: https://reactjs.org/
